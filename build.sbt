name := "docker-publisher"
ThisBuild / scalaVersion := "2.13.6"
ThisBuild / organization     := "no.uit.sfb"
ThisBuild / organizationName := "SfB"
ThisBuild / maintainer := "mma227@uit.no"

lazy val gitCommit = settingKey[String]("Git SHA")
lazy val gitRefName = settingKey[String]("Git branch or tag")

useJGit

ThisBuild / gitCommit := { sys.env.getOrElse("CI_COMMIT_SHA", "unknown")}
ThisBuild / gitRefName := { sys.env.getOrElse("CI_COMMIT_REF_NAME", git.gitCurrentBranch.value) }
ThisBuild / version := gitRefName.value //Do not use slug here as it would replace '.' with '-'
ThisBuild / resolvers += "gitlab" at "https://gitlab.com/api/v4/groups/2067095/-/packages/maven"

val scalaUtilsVer = "0.3.1"

libraryDependencies ++= Seq(
  "com.github.scopt" %% "scopt" % "4.0.1",
  "no.uit.sfb" %% "scala-utils-common" % scalaUtilsVer,
  "no.uit.sfb" %% "scala-utils-yaml" % scalaUtilsVer,
  "no.uit.sfb" %% "ref-db" % "master",
  "ch.qos.logback" % "logback-classic" % "1.2.10",
  "com.github.blemale" %% "scaffeine" % "5.1.2" % "compile"
)

enablePlugins(JavaAppPackaging, UniversalPlugin, BuildInfoPlugin)

publishConfiguration := publishConfiguration.value.withOverwrite(true) //Always allow overwrite since Scala API documentation gets published by docker:publish somehow
useCoursier := false

buildInfoKeys := Seq[BuildInfoKey](
  name,
  version,
  scalaVersion,
  sbtVersion,
  gitCommit
)
buildInfoPackage := s"${organization.value}.info.${name.value.replace('-', '_')}"
