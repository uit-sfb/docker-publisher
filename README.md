# Docker publisher

Tool for publishing multiple Docker images.

## Directory structure

Each folder under `tools` or `database` yield one Docker image but potentially multiple tags.

The directory structure of each tool/database is as follows:
tool/db
  - versionX+
    - base
      - /Dockerfile
    - sub-tool1
      - /Dockerfile
    - sub-tool2
      - /Dockerfile
    - /dlp-conf.yaml
  - versionY+
    - ...
  - /ci.yml
  
where items starting with `/` are files while the others are directories.

`versionX+` gathers the Dockerfiles for the tool/db's version `X` and above up to version `Y` (but can stop before).

`dlp-conf.yaml` has the following structure:
```yaml
versions:
  - x.y.z
  - x'.y'.z'
flags:
  - artifact
```
The `versions` define which versions should be built and publish. The provided versions should be in the range defined by the folder they belong to.
For instance if the conf file is under `X+` then `X` <= `x.y.z` < `Y`.

Zero or more flags can be provided. Here is the list of supported flags:
- `artifact`: an artifact containing the content of the Docker image should be pushed to artifactory (for db only).

The tag of the built image is as follows: {version}{"" if base or _sub-tool}

### Example

Given the following structure:
myTool
  - 1.2+
    - base
      - /Dockerfile
    - sub-tool1
      - /Dockerfile
    - sub-tool2
      - /Dockerfile
    - /dlp-conf.yaml #versions = [1.2, 1.3]
  - 2.0+
    - base
      - /Dockerfile
    - sub-tool1
      - /Dockerfile
    - sub-tool2
      - /Dockerfile
    - /dlp-conf.yaml #versions = [2.1]
  - /ci.yml

the following images would be built:
- myTool:1.2
- myTool:1.2_sub-tool1
- myTool:1.2_sub-tool2
- myTool:1.3
- myTool:1.3_sub-tool1
- myTool:1.3_sub-tool2
- myTool:2.1
- myTool:2.1_sub-tool1
- myTool:2._sub-tool2
