package no.uit.sfb

import java.nio.file.Path

case class ImageDef(tool: String,
                    version: String,
                    subTool: String, //Empty if base
                    dockerfile: Option[Path],
                    tc: ToolConf,
                    intermediate: Option[String] = None) {
  def imageName(ignoreIntermediate: Boolean = false) = {
    val subToolStr =
      if (subTool.nonEmpty)
        s"_$subTool"
      else ""
    val intermStr =
      if (intermediate.getOrElse("").nonEmpty && !ignoreIntermediate)
        s"_${intermediate.get}"
      else ""
    s"$tool:$version$subToolStr$intermStr"
  }
}
