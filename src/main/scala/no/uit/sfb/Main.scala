package no.uit.sfb

import java.io.File

import no.uit.sfb.info.docker_publisher.BuildInfo
import scopt.OParser

object Main extends App {
  val name = BuildInfo.name
  val ver = BuildInfo.version
  val gitCommitId = BuildInfo.gitCommit

  try {
    val mainBuilder = OParser.builder[Config]

    val parserMain = {
      import mainBuilder._
      OParser.sequence(
        programName(name),
        head(name, ver),
        head(s"git: $gitCommitId"),
        help('h', "help")
          .text("prints this usage text"),
        version('v', "version")
          .text("prints the version"),
        cmd("build")
          .action((_, c) => c.copy(cmd = "build"))
          .text("Build images"),
        cmd("push")
          .action((_, c) => c.copy(cmd = "push"))
          .text("Push image"),
        cmd("publish-artifact")
          .action((_, c) => c.copy(cmd = "publish-artifact"))
          .text("Publish artifact"),
        cmd("release")
          .action((_, c) => c.copy(cmd = "release"))
          .text("Release images (build, push and publish artifact)."),
        opt[Unit]("force")
          .action((_, c) => c.copy(force = true))
          .text("Override images already pushed."),
        opt[Unit]("no-cache")
          .action((_, c) => c.copy(nocache = true))
          .text("Build without using the cache."),
        opt[String]("registry")
          .action((v, c) => c.copy(registry = v))
          .text("Docker registry."),
        opt[File]("refdb")
          .action((p, c) => c.copy(refdb = p.toPath))
          .text("Destination directory for ref-dbs."),
        arg[File]("path")
          .action((p, c) => c.copy(path = p.toPath))
          .text("Path")
      )
    }

    OParser.parse(parserMain, args, Config()) match {
      case Some(c) =>
        val dc = new DockerfileCollector(c.path)
        val dr = new DockerRegistry(c.registry)
        c.cmd match {
          case "build" =>
            dc.imageDefs foreach { id =>
              dr.build(id, c.force, c.nocache)
            }
          case "push" =>
            dc.imageDefs foreach { id =>
              dr.push(id, c.force)
            }
          case "publish-artifact" =>
            dc.imageDefs foreach { id =>
              dr.publishArtifact(id, c.refdb, c.force)
            }
          case "release" =>
            dc.imageDefs foreach { id =>
              dr.build(id, c.force, c.nocache)
              dr.push(id, c.force)
              dr.publishArtifact(id, c.refdb, c.force)
            }
        }
      case _ =>
        // arguments are bad, error message will have been displayed
        throw new IllegalArgumentException()
    }
  } catch {
    case e: Throwable =>
      println(e.toString)
      println(e.printStackTrace())
      sys.exit(1)
  }
}
