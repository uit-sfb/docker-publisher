package no.uit.sfb

import java.nio.file.{Path, Paths}

case class Config(cmd: String = "",
                  path: Path = Paths.get("."),
                  refdb: Path = Paths.get("/refdb"),
                  force: Boolean = false,
                  registry: String = "",
                  nocache: Boolean = false)
