package no.uit.sfb

import java.nio.file.Path

import no.uit.sfb.scalautils.common.FileUtils

class DockerfileCollector(path: Path) {
  private val confFileName = "dlp-conf.yaml"
  private val absolutePath = path.toAbsolutePath.normalize()
  val imageDefs: Seq[ImageDef] = {
    val imageName = absolutePath.getFileName.toString
    FileUtils.ls(path)._1 flatMap { pv =>
      val (subToolPath, files) = FileUtils.ls(pv)
      val oConfPath = files
        .find(_.getFileName.toString == confFileName)
      oConfPath match {
        case Some(confPath) =>
          val toolConf = ToolConf.parse(confPath)
          val sortedSubToolPath
            : Seq[Path] = { //We extract base and put it in head
            val (base, others) = subToolPath partition {
              _.getFileName.toString == "base"
            }
            base.headOption match {
              case Some(basePath) => basePath +: others
              case None           => others
            }
          }
          val imageDefsFromDockerfile = sortedSubToolPath flatMap { p =>
            val oDockerfilePath = FileUtils
              .ls(p)
              ._2
              .find {
                _.getFileName.toString == "Dockerfile"
              }
            val regexp = """FROM[ \t]+[\w:./-]+[ \t]+as[ \t]+(\w+)""".r
            val intermediates: Seq[String] = oDockerfilePath map {
              dockerfilePath =>
                (FileUtils.readFile(dockerfilePath).split('\n') map {
                  case regexp(as) => Some(as)
                  case _          => None
                }).toSeq.flatten
            } getOrElse Seq()
            val subToolName =
              if (p.getFileName.toString == "base") ""
              else p.getFileName.toString
            toolConf.versions flatMap { ver =>
              ImageDef(
                imageName,
                ver,
                subToolName,
                oDockerfilePath,
                toolConf,
                None
              ) +: (intermediates map { interm =>
                ImageDef(imageName,
                         ver,
                         subToolName,
                         oDockerfilePath,
                         toolConf.copy(
                           flags = toolConf.flags.filterNot(_ == "artifact")
                         ),
                         Some(interm))
              })
            }
          }
          if (imageDefsFromDockerfile.isEmpty)
            toolConf.versions map { ver =>
              ImageDef(imageName,
                       ver,
                       "",
                       None,
                       toolConf.copy(
                         flags = toolConf.flags.filterNot(_ == "artifact")
                       ))
            } else
            imageDefsFromDockerfile
        case None => Seq()
      }
    }
  }
}
