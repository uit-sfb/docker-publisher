package no.uit.sfb

import java.nio.file.Path

import no.uit.sfb.scalautils.yaml.Yaml

case class ToolConf(versions: Seq[String],
                    flags: Seq[String] = Seq())

object ToolConf {
  def parse(path: Path): ToolConf = {
    val rawToolConf = Yaml.fromFileAs[ToolConf](path)
    rawToolConf.copy(
      flags = if (rawToolConf.flags != null) rawToolConf.flags else Seq()
    )
  }
}
