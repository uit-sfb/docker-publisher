package no.uit.sfb

import java.nio.file.{Path, Paths}
import com.github.blemale.scaffeine.Scaffeine
import com.typesafe.scalalogging.LazyLogging
import no.uit.sfb.metapipe.{PackageActions, PackagesConfig}
import no.uit.sfb.scalautils.common.SystemProcess

import scala.util.{Failure, Success, Try}

class DockerRegistry(registry: String) extends LazyLogging {
  implicit val log = logger

  SystemProcess //This is to be able to get manifests from Gitlab
    .from(
      Seq(
        "bash",
        "-c",
        s"""if ! grep -q experimental ${sys
          .env("HOME")}/.docker/config.json; then sed -i '$$s/}/,\\n"experimental": "enabled"}/' ${sys
          .env("HOME")}/.docker/config.json; fi"""
      )
    )
    .exec()

  protected def fullImageName(image: String) = s"$registry/$image"

  protected val exsistCache =
    Scaffeine().build[String, Boolean](
      (imageName: String) =>
        Try {
          SystemProcess(s"docker manifest inspect $imageName").exec()
        } match {
          case Success(_) => true
          case Failure(_) => false
      }
    )

  def build(imageDef: ImageDef,
            force: Boolean = false,
            noCache: Boolean = false) = {
    imageDef.dockerfile map { dockerfile =>
      if (!exists(imageDef) || force) {
        val contextDir = dockerfile.getParent
        val ver = imageDef.version
        val imageName = fullImageName(imageDef.imageName())
        val targetOpt = imageDef.intermediate
          .map { interm =>
            s"--target $interm"
          }
          .getOrElse("")
        val noCacheFlag = if (noCache) "--no-cache" else ""
        SystemProcess(
          s"""docker build -t $imageName --pull $noCacheFlag $targetOpt --build-arg version=$ver .""",
          path = Some(contextDir)
        ).exec()
      }
    }
  }

  def push(imageDef: ImageDef, force: Boolean = false) = {
    imageDef.dockerfile map { dockerfile =>
      if (imageDef.intermediate.isEmpty) { //We do not push intermediate images
        if (!exists(imageDef) || force) {
          val imageName = fullImageName(imageDef.imageName())
          if (exsistCache.get(imageName))
            println(s"Overriding image $imageName")
          SystemProcess(s"""docker push $imageName""").exec()
        }
      }
    }
  }

  def publishArtifact(imageDef: ImageDef,
                      refdb: Path,
                      force: Boolean = false) = {
    imageDef.dockerfile map { dockerfile =>
      if (imageDef.intermediate.isEmpty && imageDef.tc.flags.contains(
            "artifact"
          )) { //We do not push intermediate images nor images not flagged as artifact
        val imageName = fullImageName(imageDef.imageName())
        PackageActions(
          PackagesConfig(
            "create",
            imageName = imageName,
            dataDir = Some(refdb.toAbsolutePath.toString),
            overwrite = force
          )
        )
      }
    }
  }

  protected def exists(imageDef: ImageDef): Boolean = {
    val imageName = fullImageName(imageDef.imageName(true))
    val res = exsistCache.get(imageName)
    if (res)
      println(s"$imageName already exists -- skipping action")
    res
  }
}
